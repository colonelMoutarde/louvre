
$(function () {

    var dteNow = new Date();
    var hour = dteNow.getHours();
    var intYear = dteNow.getFullYear();
    var nextYear = intYear + 1;
    var now = moment();
    var dateValue = $("#booking_datetimeVisit").val();

    displayAfternoonCheckbox(moment(dateValue, "DD-MM-YYYY"), now, hour);

    $('.datepicker').datepicker({

        datesDisabled: [
            '01/05/' + intYear, '01/05/' + nextYear,
            '01/11/' + intYear, '01/11/' + nextYear,
            '25/12/' + intYear, '25/12/' + nextYear
        ],
        clearBtn: true,
        language: "fr",
        maxViewMode: 2,
        daysOfWeekDisabled: "2",
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true
    });

    //count Visitors

    $("#booking_datetimeVisit").change(function () {

        var dateValue = $("#booking_datetimeVisit").val();
        //toogle hide afternoon

        var dateFromForm = moment(dateValue, "DD-MM-YYYY");
        displayAfternoonCheckbox(dateFromForm, now, hour);
 
        $.ajax({
            url: $("#booking_datetimeVisit").data("url"),
            method: "post",
            data: {'date': dateValue},
            dataType: "json",
            success: function (code_json, statut) {

                $("#counted-visitor").html(code_json['slotFree']);
            },
            error: function (resultat, statut, erreur) {
                 
                $("#error-count").html("<p>Impossible de connaître le nombre de places disponibles le " + dateValue + "</p>");
            }
        });
    });

});

function displayAfternoonCheckbox(dateFromForm, now, hour) {
    if ((dateFromForm.format('L') === now.format('L')) && hour >= 14) {
        $("#afternoon").addClass('nodisplay');
    } else {
        $("#afternoon").removeClass('nodisplay');
    }
}