<?php

namespace Louvre\TicketBundle\Test\Entity;

use Louvre\TicketBundle\Entity\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{

    public function testGetFirstName()
    {
        $order = (new Order())->setFirstName('toto');
        $this->assertEquals('toto', $order->getFirstName());
    }
    
    public function testGetEmail()
    {
        $order = (new Order())->setEmail('toto@toto.fr');
        $this->assertEquals('toto@toto.fr', $order->getEmail());
    }
    
}