<?php

namespace Louvre\TicketBundle\Test\Ressource\Services;

use Louvre\TicketBundle\Services\StripeService;
use PHPUnit\Framework\TestCase;
use Louvre\TicketBundle\Exception\PaymentException;

class StripeServiceTest extends TestCase
{

    private $secretKey = 'sk_test_7aDcpJnAgz1KwoydfHsAtzf6';
    
    public function testChargeOk()
    {

        $stripe = (new StripeService($this->secretKey))
                ->charge(100, 'tok_visa', 'test@gmail.com');

        $this->assertEquals(null, $stripe->failure_code);
    }

    public function testChargeFail()
    {
        $this->expectException(PaymentException::class);

        (new StripeService($this->secretKey))
                ->charge(100, 'tok_chargeDeclined', 'test@gmail.com');
    }

}
