<?php

namespace Louvre\TicketBundle\Test\Ressource\Services;

use Louvre\TicketBundle\Services\ApplyDiscountService;
use Louvre\TicketBundle\Entity\Order;
use Louvre\TicketBundle\Entity\Ticket;
use PHPUnit\Framework\TestCase;

class ApplyDiscountServiceTest extends TestCase
{

    public function testApplyDiscount()
    {
        $order = (new Order())
                ->setDatetimeVisit(new \DateTime('2018-01-19'))
                ->setTickets([(new Ticket())
            ->setPrice(16.00)
            ->setBirthDate(new \DateTime('2000-01-19'))]);

        (new ApplyDiscountService(20))
                ->applyDiscount($order);

        $this->assertEquals(16, $order->getTickets()[0]->getPrice());
    }

    public function testApplyDiscountAfternoon()
    {
        $order = (new Order())
                ->setDatetimeVisit(new \DateTime('2018-01-19'))
                ->setAfternoon(true)
                ->setTickets([(new Ticket())
            ->setPrice(16.00)
            ->setBirthDate(new \DateTime('2000-01-19'))]);

        (new ApplyDiscountService(20))
                ->applyDiscount($order);

        $this->assertEquals(8, $order->getTickets()[0]->getPrice());
    }

    public function testApplyDiscountChildren()
    {
        $order = (new Order())
                ->setDatetimeVisit(new \DateTime('2018-01-19'))
                ->setTickets([(new Ticket())
            ->setPrice(16.00)
            ->setBirthDate(new \DateTime('2013-01-19'))]);

        (new ApplyDiscountService(20))
                ->applyDiscount($order);

        $this->assertEquals(8, $order->getTickets()[0]->getPrice());
    }
    
    public function testApplyDiscountBaby()
    {
        $order = (new Order())
                ->setDatetimeVisit(new \DateTime('2018-01-19'))
                ->setTickets([(new Ticket())
            ->setPrice(16.00)
            ->setBirthDate(new \DateTime('2017-01-19'))]);

        (new ApplyDiscountService(20))
                ->applyDiscount($order);

        $this->assertEquals(0, $order->getTickets()[0]->getPrice());
    }
    
    public function testApplyDiscountOldtimer()
    {
        $order = (new Order())
                ->setDatetimeVisit(new \DateTime('2018-01-19'))
                ->setTickets([(new Ticket())
            ->setPrice(16.00)
            ->setBirthDate(new \DateTime('1911-01-19'))]);

        (new ApplyDiscountService(20))
                ->applyDiscount($order);

        $this->assertEquals(12, $order->getTickets()[0]->getPrice());
    }

    public function testApplyDiscountMilitary()
    {
        $order = (new Order())
                ->setDatetimeVisit(new \DateTime('2018-01-19'))
                ->setTickets([(new Ticket())
                ->setDiscount(true)
            ->setPrice(16.00)
            ->setBirthDate(new \DateTime('1981-10-19'))]);

        (new ApplyDiscountService(20))
                ->applyDiscount($order);

        $this->assertEquals(10, $order->getTickets()[0]->getPrice());
    }

}
