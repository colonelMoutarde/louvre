<?php

namespace Louvre\TicketBundle\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TicketControllerTest extends WebTestCase
{

    public function testAddAction()
    {
        $client = static::createClient();

        $client->request('GET', '/ticket');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

}
