<?php

namespace Louvre\TicketBundle\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{

    private $date;

    private function dateNextYear()
    {
        $this->date = (new \DateTime())->modify('+1 year');
        return $this->date;
    }

    public function testIndexAction()
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateOrder()
    {
        $client  = static::createClient();
        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Valider')->form([
            'booking[datetimeVisit]'    => $this->dateNextYear()->format('d/m/Y'),
            'booking[numberOfVisitors]' => '1',
            'booking[email]'            => 'op@luc-sanchez.fr',
        ]);

        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testCreateOrderAfretnoon()
    {
        $client  = static::createClient();
        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Valider')->form([
            'booking[datetimeVisit]'    => $this->dateNextYear()->format('d/m/Y'),
            'booking[numberOfVisitors]' => '1',
            'booking[afternoon]'        => '1',
            'booking[email]'            => 'op@luc-sanchez.fr',
        ]);

        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testCompleteCreateOrderAfternoonWithClient()
    {
        $client  = static::createClient();
        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Valider')->form([
            'booking[datetimeVisit]'    => $this->dateNextYear()->format('d/m/Y'),
            'booking[numberOfVisitors]' => '1',
            'booking[afternoon]'        => '1',
            'booking[email]'            => 'op@luc-sanchez.fr',
        ]);

        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawlerSetClient = $client->followRedirect();

        $birthDate = (new \DateTime())->modify('-18 year');

        $formSetClient = $crawlerSetClient->selectButton('Valider')->form([
            'visite[visitor][Visiteur_1][country]'          => 'DE',
            'visite[visitor][Visiteur_1][firstname]'        => 'jurgen',
            'visite[visitor][Visiteur_1][lastname]'         => 'röech',
            'visite[visitor][Visiteur_1][birthdate][day]'   => $birthDate->format('d'),
            'visite[visitor][Visiteur_1][birthdate][month]' => $birthDate->format('m'),
            'visite[visitor][Visiteur_1][birthdate][year]'  => $birthDate->format('Y'),
            'visite[visitor][Visiteur_1][discount]'         => '1',
        ]);
        $client->submit($formSetClient);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}
