<?php

namespace Louvre\TicketBundle\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PaymentControllerTest extends WebTestCase
{

    public function testRecapAction()
    {
        $client = static::createClient();

        $client->request('GET', '/payment-recap-run');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

}
