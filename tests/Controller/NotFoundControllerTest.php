<?php

namespace Louvre\TicketBundle\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NotFoundControllerTest extends WebTestCase
{

    public function testNotfoundPage()
    {
        $client   = static::createClient();
        $client->request('GET', '/skflmskflsmkflms');
        $response = $client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

}
