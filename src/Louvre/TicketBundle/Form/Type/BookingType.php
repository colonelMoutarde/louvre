<?php

namespace Louvre\TicketBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Louvre\TicketBundle\Validator\Constraints as CustomAssert;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class BookingType extends AbstractType
{

    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('datetimeVisit', DateType::class, [
                    'label'       => 'Date de la réservation',
                    'widget'      => 'single_text',
                    'attr'        => [
                        'class'    => 'datepicker',
                        'data-url' => $this->router->generate('louvre_ticket_count_visitor')
                    ],
                    'format'      => 'dd/MM/yyyy',
                    'required'    => true,
                    'constraints' => [
                        new Assert\NotBlank(),
                        new Assert\GreaterThanOrEqual('today'),
                        new CustomAssert\ConstraintInvalidDay([2]),
                        new CustomAssert\ConstraintDayOff(['05-01', '01-11', '12-25']),
                    ],
                ])
                ->add('afternoon', CheckboxType::class, [
                    'label'    => 'Vous venez l\'après midi',
                    'required' => false,
                ])
                ->add('numberOfVisitors', IntegerType::class, [
                    'label'       => 'Combien serez vous ?',
                    'constraints' => new Assert\GreaterThanOrEqual(1),
                    'required'    => true,
                ])
                ->add('email', EmailType::class, [
                    'label'       => 'votre email',
                    'required'    => true,
                    'constraints' => new Assert\Email(['checkMX' => true]),
                ]) ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Louvre\TicketBundle\Entity\Order',
        ));
    }

}
