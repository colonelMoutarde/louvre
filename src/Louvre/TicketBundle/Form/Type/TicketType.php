<?php

namespace Louvre\TicketBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class TicketType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('country', CountryType::class, [
                    'label'       => 'Pays du visiteur',
                    'required'    => true,
                    'data'        => 'FR',
                    'multiple'    => false,
                    'expanded'    => false,
                    'constraints' => new Assert\Country(),
                ])
                ->add('firstname', TextType::class, [
                    'label'       => 'Votre prénom',
                    'required'    => true,
                    'constraints' => new Assert\NotBlank(),
                ])
                ->add('lastname', TextType::class, [
                    'label'       => 'Votre nom',
                    'required'    => true,
                    'constraints' => new Assert\NotBlank(),
                ])
                ->add('birthdate', BirthdayType::class, [
                    'label'       => 'Date de naissance',
                    'data'        => (new \DateTime())->modify('-18 year'),
                    'format'      => 'dd-MM-yyyy',
                    'required'    => true,
                    'constraints' => new Assert\Date(),
                ])
                ->add('discount', ChoiceType::class, [
                    'label'    => 'Bénéficiez vous d\'une réduction',
                    'choices'  => [
                        'oui' => true,
                        'non' => false,
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'required' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Louvre\TicketBundle\Entity\Ticket'
        ]);
    }

}
