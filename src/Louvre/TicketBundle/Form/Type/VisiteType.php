<?php

namespace Louvre\TicketBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class VisiteType extends AbstractType
{

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('visitor', CollectionType::class, [
            'label'        => 'Visiteurs',
            'entry_type'   => TicketType::class,
            'allow_add'    => true,
            'allow_delete' => true,
        ]);
    }

}
