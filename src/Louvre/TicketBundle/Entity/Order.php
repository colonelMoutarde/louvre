<?php

namespace Louvre\TicketBundle\Entity;

use Louvre\TicketBundle\Validator\Constraints\ConstraintOverCapacity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Card
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="Louvre\TicketBundle\Repository\OrderRepository")
 * @ConstraintOverCapacity()
 */
class Order
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=320)
     */
    private $email;

    /**
     * @var string dateTime
     * 
     * @ORM\Column(name="datetime_visit", type="date" )
     */
    private $datetimeVisit;

    /**
     * @ORM\OneToMany(targetEntity="Louvre\TicketBundle\Entity\Ticket", mappedBy="order", cascade={"persist"} )
     * 
     * @ORM\JoinColumn(nullable=true)
     */
    private $tickets;

    /**
     *
     * @var bool default false 
     * @ORM\Column(name="afternoon", type="boolean" )
     */
    private $afternoon = false;

    /**
     * @var string
     *
     * @ORM\Column(name="booking_code", type="string", length=255, unique=true)
     */
    private $bookingCode;

    /**
     *
     * @var int default 1 
     * @ORM\Column(name="number_of_visitor", type="integer")
     */
    private $numberOfVisitors  = 1;
    
    /**
     *
     * @var bool default false 
     * @ORM\Column(name="accepted_payment", type="boolean" )
     */
    private $acceptedPayment = false;

    private $totalPrice = 0;
    
    private $defaultPrice      = 16.00;
    private $specialOffDivider = 2;

    public function __construct()
    {
        $this->datetimeVisit = new \Datetime();
        $this->bookingCode   = uniqid();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Card
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Card
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Card
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function getDatetimeVisit()
    {
        return $this->datetimeVisit;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setDatetimeVisit(\DateTime $datetimeVisit)
    {
        $this->datetimeVisit = $datetimeVisit;
        return $this;
    }

    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * 
     * @param type $tickets
     * @return $this
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;
        return $this;
    }

    public function getAfternoon()
    {
        return $this->afternoon;
    }

    /**
     * 
     * @param type $afternoon
     * @return $this
     */
    public function setAfternoon($afternoon)
    {
        $this->afternoon = $afternoon;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getNumberOfVisitors(): int
    {
        return $this->numberOfVisitors;
    }

    /**
     * 
     * @param int $numberOfVisitors
     * @return $this
     */
    public function setNumberOfVisitors(int $numberOfVisitors)
    {
        $this->numberOfVisitors = $numberOfVisitors;
        return $this;
    }

    public function getSpecialOffDivider()
    {
        return $this->specialOffDivider;
    }

    public function getBookingCode()
    {
        return $this->bookingCode;
    }

    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(float $totalPrice)
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }


    public function getAcceptedPayment()
    {
        return $this->acceptedPayment;
    }

    public function setAcceptedPayment($acceptedPayment)
    {
        $this->acceptedPayment = $acceptedPayment;
        return $this;
    }

    
}
