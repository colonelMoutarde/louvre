<?php

namespace Louvre\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="ticket")
 * @ORM\Entity(repositoryClass="Louvre\TicketBundle\Repository\TicketRepository")
 */
class Ticket
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tickets_code", type="string", unique=true)
     */
    private $ticketCode;

    /**
     * @var string dateTime
     * 
     * @ORM\Column(name="birthdate", type="date" )
     */
    private $birthDate;

    /**
     *
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=2)
     */
    private $price;

    /**
     *
     * @var float
     *
     * @ORM\Column(name="tax", type="float", precision=2)
     */
    private $tax;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=2)
     */
    private $country;

    /**
     *
     * @ORM\ManyToOne (targetEntity="Louvre\TicketBundle\Entity\Order", inversedBy="tickets")
     * @ORM\JoinColumn( nullable=false)
     */
    private $order;

    /**
     *
     * @var bool
     * @ORM\Column(name="discount", type="boolean") 
     */
    private $discount = false;

    public function __construct()
    {
        $this->ticketCode = uniqid(); //changer à ticketCode
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookingCode
     * 
     * @param string $ticketCode
     * @return $this
     */
    public function setTicketCode(string $ticketCode)
    {
        $this->ticketCode = $ticketCode;

        return $this;
    }

    /**
     * Get ticketCode
     *
     * @return string
     */
    public function getTicketCode(): string
    {
        return $this->ticketCode;
    }

    /**
     * Get Birthdate
     * 
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * 
     * @param \DateTime $birthDate
     * @return $this
     */
    public function setBirthDate(\DateTime $birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * 
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
        return $this;
    }

    public function getTax()
    {
        return $this->tax;
    }

    public function setTax(float $tax)
    {
        $this->tax = $tax;
        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function getDiscount() :bool
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }  

}
