<?php

namespace Louvre\TicketBundle\Services;

use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Error\Card;
use Stripe\Error\Base;
use Stripe\Error\RateLimit;
use Stripe\Error\ApiConnection;
use Stripe\Error\Authentication;
use Stripe\Error\InvalidRequest;
use Symfony\Component\HttpFoundation\RequestStack;
use Louvre\TicketBundle\Exception\PaymentException;

/**
 * Description of StripeService
 *
 * @author Luc
 */
class StripeService
{

    private $privateKeyStripe;

    /**
     * 
     * @param string $privateKeyStripe
     */
    public function __construct(string $privateKeyStripe)
    {
        $this->privateKeyStripe = $privateKeyStripe;
    }

    /**
     * 
     * @param float $price
     * @param string $token
     * @return boolean
     */
    public function charge(float $price, string $token, string $email)
    {

        Stripe::setApiKey($this->privateKeyStripe);

        try {
            return Charge::create([
                    "amount"        => (int) (100 * $price),
                    "currency"      => 'eur',
                    "source"        => $token,
                    "capture"       => false,
                    "description"   => 'Ticket du Musée du Louvre',
                    "receipt_email" => $email,

                ]);
 
        } catch (Card | InvalidRequest | RateLimit | Authentication | ApiConnection | Base $exc) {
            $body = $exc->getJsonBody();
            $err  = $body['error'];
            throw new PaymentException($err['message']);
        }
         
    }

}
