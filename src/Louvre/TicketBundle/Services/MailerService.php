<?php

namespace Louvre\TicketBundle\Services;

class MailerService
{

    private $mailer;
    private $from;

    /**
     * 
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer, string $from)
    {
        $this->mailer = $mailer;
        $this->from   = $from;
    }

    /**
     * 
     * @param string $subject
     * @param string $to
     * @param string $body
     */
    public function sendMessage(string $subject, string $to, string $body)
    {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->from)
                ->setTo($to)
                ->setBody($body, 'text/html');

        $this->mailer->send($message);
    }

}
