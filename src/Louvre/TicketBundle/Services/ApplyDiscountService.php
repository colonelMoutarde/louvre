<?php

namespace Louvre\TicketBundle\Services;

use Louvre\TicketBundle\Entity\Order;

/**
 * Description of ApplyDiscountService
 *
 * @author luc
 */
class ApplyDiscountService
{

    private $tax;

    public function __construct(float $tax)
    {
        $this->tax = $tax;
    }

    /**
     * 
     * @param Order $order
     */
    public function applyDiscount(Order $order)
    {
         foreach ($order->getTickets() as $ticket) {

            $age = $order->getDatetimeVisit()->diff($ticket->getBirthDate())->y;

            $ticket->setTax($this->tax);

            switch ($age) {
                case $age <= 4:
                    $ticket->setPrice(0.00);
                    break;
                case ($age >= 4 && $age < 12 ):
                    $ticket->setPrice(8.00);
                    break;
                case $age >= 60:
                    $ticket->setPrice(12.00);
                    break;
                default:
                    $ticket->setPrice(16.00);
                    break;
            }

            if ($ticket->getDiscount()) {
                $ticket->setPrice(10.00);
            }

            if ($order->getAfternoon()) {
                $ticket->setPrice($ticket->getPrice() / 2);
            }
        }
                
    }

}
