<?php

namespace Louvre\TicketBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Louvre\TicketBundle\Entity\Ticket;
use Doctrine\ORM\EntityManagerInterface;

class ConstraintOverCapacityValidator extends ConstraintValidator
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     * 
     * @param type $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $currentCapacity = $this->em->getRepository(Ticket::class)->countVisitorPerDay($value->getDatetimeVisit()) + $value->getNumberOfVisitors();
        
        if ( $currentCapacity >= $constraint->maxCapacity) {
            $this->context->buildViolation($constraint->message)
                    ->addViolation();
        }
    }

    

}
