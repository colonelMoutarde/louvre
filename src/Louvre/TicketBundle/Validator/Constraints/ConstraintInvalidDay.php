<?php

namespace Louvre\TicketBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ConstraintInvalidDay extends Constraint
{

    /**
     *
     * @var string 
     */
    public $message = 'Il n\'est pas possible de venir ce jour.';
    
    private $numberOfInvalidDays;

    /**
     * 
     * @param array $array
     */
    public function __construct(array $array = [])
    {
        $this->numberOfInvalidDays = $array;
    }

    /**
     * 
     * @return string
     */
    public function validatedBy(): string
    {
        return get_class($this) . 'Validator';
    }

    /**
     * 
     * @return array
     */
    public function getNumberOfInvalidDays(): array
    {
        return $this->numberOfInvalidDays;
    }

    /**
     * 
     * @param array $numberOfInvalidDays
     * @return $this
     */
    public function setNumberOfInvalidDays(array $numberOfInvalidDays)
    {
        $this->numberOfInvalidDays = $numberOfInvalidDays;
        return $this;
    }

}
