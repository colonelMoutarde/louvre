<?php

namespace Louvre\TicketBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class ConstraintDayOff extends Constraint
{

    private $dayOff;

    /**
     *
     * @var string 
     */
    public $message = 'Il n\'est pas possible de sélectionner ce jour férié';

    /**
     * 
     * @param array $array
     */
    public function __construct(array $array = [])
    {
        $this->dayOff = $array;
    }

    /**
     * 
     * @return array
     */
    public function getDayOff(): array
    {
        return $this->dayOff;
    }

    /**
     * 
     * @param array $dayOff
     * @return $this
     */
    public function setDayOff(array $dayOff)
    {
        $this->dayOff = $dayOff;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

}
