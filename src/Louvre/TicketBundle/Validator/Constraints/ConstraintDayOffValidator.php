<?php

namespace Louvre\TicketBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class ConstraintDayOffValidator extends ConstraintValidator
{

    /**
     * 
     * @param string $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        if (in_array($value->format('m-d'), $constraint->getDayOff())) {
            $this->context->buildViolation($constraint->message)
                    ->addViolation();
        }
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
