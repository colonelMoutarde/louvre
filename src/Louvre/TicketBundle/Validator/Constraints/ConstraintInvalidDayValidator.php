<?php

namespace Louvre\TicketBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConstraintInvalidDayValidator extends ConstraintValidator
{

    /**
     * 
     * @param type $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (in_array($value->format('w'), $constraint->getNumberOfInvalidDays())) {
            $this->context
                    ->buildViolation($constraint->message)
                    ->addViolation();
        }
    }

    /**
     * 
     * @return type
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
