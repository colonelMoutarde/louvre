<?php

namespace Louvre\TicketBundle\Validator\Constraints;
use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */
class ConstraintOverCapacity extends Constraint
{

    public $message = 'Il n\'y a pas assez de places disponibles ce jour';
    
    public $maxCapacity = 1000;
    

    /**
     * 
     * @return type
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    /**
     * 
     * @return string
     */
    public function validatedBy()
    {
        return 'validator.over_capacity';
    }

}
