<?php

namespace Louvre\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Louvre\TicketBundle\Entity\Order;
use Louvre\TicketBundle\Entity\Ticket;
use Louvre\TicketBundle\Form\Type\BookingType;

class IndexController extends AbstractController
{

    public function indexAction(Request $request)
    {
        $order = new Order();

        $form = $this->createForm(BookingType::class, $order);
        $form->handleRequest($request);

        $product = $this->getDoctrine()->getRepository(Ticket::class)
                ->countVisitorPerDay(new \DateTime());

        $slotFree = (1000 - (int) $product );

        if (($form->getData()->getDatetimeVisit()->format('Ymd') === (new \DateTime('now'))->format('Ymd')) && (new \DateTime('now'))->format('H') >= 14) {
                $order->setAfternoon(true);
                $afternoon = 'nodisplay';
            } else {
                $afternoon = null;
            }
        
        if ($form->isSubmitted() && $form->isValid()) {

            if ($order->getAfternoon()) {
                $this->get('session')
                        ->getFlashBag()
                        ->add('success', 'Vous benéficiez d\'une réduction de 50% en choisisant de venir l\'apres midi');
            }

            $em = $this->getDoctrine()
                    ->getManager();
            $em->persist($order);
            $em->flush();

            $request->getSession()->set('userOrder', $order);

            return $this->redirectToRoute('louvre_ticket_add');
        }

        return $this->render('LouvreTicketBundle:Louvre:index.html.twig', [
                    'formOrder' => $form->createView(),
                    'slotFree'  => $slotFree,
                    'afternoon' => $afternoon
        ]);
    }

}
