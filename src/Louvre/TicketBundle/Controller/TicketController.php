<?php

namespace Louvre\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Louvre\TicketBundle\Entity\Ticket;
use Louvre\TicketBundle\Form\Type\VisiteType;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * Description of SelectTicketController
 *
 * @author luc
 */
class TicketController extends AbstractController
{

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function addAction(Request $request)
    {
        $order = $request->getSession()->get('userOrder');

        if ($order === null) {
            return $this->redirectToRoute('louvre_ticket_homepage');
        }
        try {
            $visitors = $order->getNumberOfVisitors();
        } catch (FatalThrowableError $exc) {
            return $this->redirectToRoute('louvre_ticket_homepage');
        }

        $tickets = [];

        $em = $this->getDoctrine()
                ->getManager();

        $order = $em->merge($order);

        $em->refresh($order);
        for ($i = 0; $i < $visitors; $i++) {
            $tickets['Visiteur_' . ($i + 1)] = new Ticket();
            $tickets['Visiteur_' . ($i + 1)]->setOrder($order);
        }

        $form = $this->createForm(VisiteType::class, ['visitor' => $tickets]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $order->setTickets($tickets);

            $this->get('louvre_ticket.apply_discount_service')->applyDiscount($order);

            $em->flush();

            $request->getSession()->set('userOrder', $order);

            return $this->redirectToRoute('louvre_recap_payment');
        }

        return $this->render('LouvreTicketBundle:Louvre:ticket.html.twig', [
                    'ticketForm' => $form->createView(),
        ]);
    }

}
