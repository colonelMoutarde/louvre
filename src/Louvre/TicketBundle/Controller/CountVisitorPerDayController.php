<?php

namespace Louvre\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Louvre\TicketBundle\Entity\Ticket;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CountVisitorPerDayController extends AbstractController
{

    /**
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function countVisitorAction(Request $request)
    {

        if ($request->isXmlHttpRequest()) {

            $date = \DateTime::createFromFormat('d/m/Y', $request->request->get('date'));
             
            $product = $this->getDoctrine()->getRepository(Ticket::class)
                    ->countVisitorPerDay($date);
 
            $slotFree = (1000 - (int) $product );
            return new JsonResponse(['slotFree' => (int) $slotFree]);
        } else {
            return (new Response())->setStatusCode(500);
        }
    }

}
