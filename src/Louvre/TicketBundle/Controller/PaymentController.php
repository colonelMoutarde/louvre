<?php

namespace Louvre\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Louvre\TicketBundle\Form\Type\StripePaymentType;
use Louvre\TicketBundle\Exception\PaymentException;

/**
 * Description of PaymentController
 *
 * @author luc
 */
class PaymentController extends AbstractController
{

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function recapAction(Request $request)
    {
        $order = $request->getSession()->get('userOrder');

        if ($order === null) {
            return $this->redirectToRoute('louvre_ticket_homepage');
        }

        $subTotal = $order->getTickets()->getValues();

        $bigTotal = null;
        foreach ($subTotal as $total) {
            $bigTotal += $total->getPrice();
        }

        $order->setTotalPrice($bigTotal);
        $request->getSession()->set('userOrder', $order);

        if ($bigTotal == 0) {
            $this->get('session')
                    ->getFlashBag()
                    ->add('info', 'Impossible de commander seulement des billets à 0 €');
        }

        $form = $this->createForm(StripePaymentType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $this->get('louvre_ticket.stripe_service')
                        ->charge($order->getTotalPrice(), $form->getData()['token'], $order->getEmail());

                $this->get('louvre_ticket.mailer_service')
                        ->sendMessage('Votre paiement est accepté', $order->getEmail(), $this->renderView('LouvreTicketBundle:Louvre/Email:tickets.html.twig', ['order' => $order]));

                $em    = $this->getDoctrine()
                        ->getManager();
                $order = $em->merge($order);
                $em->refresh($order);
                $order->setAcceptedPayment(true);
                $em->flush();

                $request->getSession()->remove('userOrder');
                $this->get('session')
                        ->getFlashBag()
                        ->add('info', 'Le paiment est un succés. Consultez votre messagerie, vous y trouverez vos tickets');
                
            } catch (PaymentException $exc) {
                $this->get('session')
                        ->getFlashBag()
                        ->add('error', 'Un problème est survenu lors du paiement, merci de recommencer');
                
                return $this->redirectToRoute('louvre_recap_payment');
            }

            return $this->redirectToRoute('louvre_return_url_payment');
        }

        return $this->render('LouvreTicketBundle:Louvre:recap-pay.html.twig', [
                    'order'             => $order,
                    'publicKey'         => $this->container->getParameter('stripe.publishable_key'),
                    'formStripePayment' => $form->createView(),
        ]);
    }

    public function statusAction()
    {
        return $this->render('LouvreTicketBundle:Louvre:status.html.twig');
    }

}
